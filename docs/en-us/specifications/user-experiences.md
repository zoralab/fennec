<!--
+++
title = "User Experiences"
description = "Interested to learn Fennec's design for User Experience? This is definately the page."
keywords = ["fennec", "user", "experiences", "user experiences", "design"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Wed Feb 27 20:19:43 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 2
+++
-->

# User Experiences
Fennec CLI and user interface are explictly and purposefully designed with
user experience mandates. Each of them gone through thorough usability testing
over time for growth and simplification.

Remember, the pain in developing `.gitlab-ci.yml` inspired this project. Hence,
we should learn from the lesson: the very first thing to do is to design an
easy, intuitively understandable, and comprehesible way to deploy Fennec.

<br/>

## Mandate #1 - Keep Installation Simple
Installing Fennec as a local software should be as simple as:
```bash
$ sudo apt-get install fennec -y
```
By design, user should **ONLY** be instructed to install Fennec package (
not to confuse with ***packaging tasks***) with **1-line command**. That also
means the command should setup everything including its dependencies.

### DO
1. Have a clear autonomous, scriptable, controllable way to handle execution.
2. Resolves all dependencies autonomously.
3. Practices safe dependencies and parameters checking.
4. Always do the right thing first; speed and scale comes later.

### DO NOT
1. Write a bunch of documents to let user to set things up in order to use.
That's developers' job. Period.
2. Do things without testing.
3. Do things without having a mind how to fully control it.
4. Hide expression and meaning. Just be open, direct, and straight already.

</br>

## Mandate #2 - Easy One-line Repository Setup
Setting up repository with Fennec should be as simple as:
```
$ fennec --install
```
All settings, executions, scripts, recipes, and so on should be properly
configured before deliver to any user.

### DO
1. Organize, test, and comply to this rule.
2. Keep all execution independent of yml script. Maintain **semi-automatic**
principle.

### DO NOT
1. Do things that introduces more documentation addendum (we all hate that).
2. Do anything that breaks this simple one-line instruction.

<br/>

## Mandate #3 - Simple Lines YAML Configuration Files
Fennec's objective is to keep the repository's `.gitlab-ci.yml` clean and dead
simple. Hence, it should be as neat as:
```yaml
include:
    - .fennec/core/linux/debian.yml
    - .fennec/test/linux/go.yml
    - .fennec/publish/hugo/debian.yml
    - .fennec/package/linux/go.yml
    - .fennec/upstream/linux/go.yml
```

### DO
1. Keep executions inside your fennec execution scripts.
2. Only let user to configure fennec via key variables.

### DO NOT
1. Create something and expect users to write the execution codes.
2. Expect user to configure a lot of key variables (>8). That's absurd.
3. Expect user to read any Oxford's English dictionary sized documentations.

<br/>

## Mandate #4 - Write A Human Readable Help
Fennec's help prompt should be as simple as, as an example:
```bash
$ fennec --help
Fennec
Your repository's one-stop shop for GitLab CI YAML templates.
-------------------------------------------------------------------------------
To use: ./fennec.sh [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help                   print help. Longest help is up
                                to this length for terminal
                                friendly printout.

2. -v, --version                print app version.

3. -i, --install                install the fennec library inside this
                                repository. Its default location is in the
                                current directory. It optional value is given,
                                it will only install that component.
                                OPTIONAL VALUES:
                                1. -i fennec
                                  install the gitlab-ci fennec.

                                OPTIONAL ARGUMENTS:
                                1. -p, --path [/path/to/repo]
                                  the path you wish to install fennec library.

4. -u, --uninstall              uninstall the fennec library inside this
                                repository. Its default location is in the
                                current directory. If optional value is given,
                                it will only uninstall that component.
                                OPTIONAL VALUES:
                                1. -u fennec

                                OPTIONAL ARGUMENTS:
                                1. -p, --path [/path/to/repo]
                                  the path you wish to install fennec library.

5. -U, --update                 update the fennec library inside this
                                repository. Its default location is in the
                                current directory. If optional value is given,
                                it will only update that component.
                                OPTIONAL VALUES:
                                1. -U fennec

                                OPTIONAL ARGUMENTS:
                                1. -p, --path [/path/to/repo]
                                  the path you wish to update fennec library.
```
Write it clear explaination in the user interface, like 80 columns for terminal
CLI. Make sure user can get all the necessary information for each actions.

### DO
1. Write it clear with explaination, tells the required values for each
arguments, relates the associated arguments to the action.
2. Differenciate action and flags.
3. Write example commands for each action.

### DO NOT
1. **Write unless texts**. That's like giving a bottle of kerosine to a thirsty
person. Please, respect people is looking for help.
2. **Disrespect user interface**. Respect the interface boundaries.
3. **Write with assumptions**. Don't expect user to know everything. They use
help for a reason.

