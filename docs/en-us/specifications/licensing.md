<!--
+++
title = "Licensing"
description = "Fennec developers' guide on code licensing."
keywords = ["licensing", "fennec", "contribute"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Mon Mar  4 15:20:19 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 6
+++
-->

# Licensing
Fennec outbound license is [Apache 2.0](https://gitlab.com/ZORALab/fennec/blob/master/LICENSE).
Hence, any new code, designs, and efforts contributed back to the project
**must** comply to the outbound license.

## Forbidden Licenses
There are the licenses forbidden from integrating into the project due to
inbound licensing incompatibilities:

1. Proprietary licenses
2. GPL Version 2 license and similar Copyleft licenses
3. Missing license
4. Unknown licenses

You must always understand where do you get the idea/codes before you apply into
the project itself.
