<!--
+++
title = "Upstream Process"
description = "Want to know what is the process to contribute code back? This is the right place."
keywords = ["Fennec", "upstream", "process"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Tue Mar  5 12:16:09 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 9
+++
-->

# Upstream Process
This page describes the necessary steps for contributing back your codes. Unless
otherwise specified, it should be standardized across all ZORALab application
processes.

<br/>

## Ground Rules
The only **one strict** requirement we impose here is your `git`
**commit message**. You **MUST** comply to our message pattern in order not to
break our build system.

### Rationale
#### #1 - CHANGELOG.md Automation
We use `git log --oneline` to generate our CHANGELOG.md for every release.
Hence, if you don't comply to our format, it will be very confusing and
possibly break the tool system.

#### #2 - We read Git Log Very Often
From our day-to-day operations, we work with terminal based interface locally
over a straight 12~20+ hours (highest record) per day. We simply do not have the
time to visit a website just to view past commits list.

#### #3 - Remove Dependency
The git log system was initially designed without these version control cloud
system such as Github or GitLab. Hence, we want to maintain such historical
tracking in our repository instead of reading git log here and
cross-referencing somewhere else.

This is code level development, let's not introduce another dependency.

<br/>

### GnuPG Commit Level
To ensure you are **you** (integrity checking), the commit should be signed
using the GnuPG system. Please ensure your local `git` system is using GnuPG
signing system before proceeding and have the public key available in the
[pgp.mit.edu](https://pgp.mit.edu/) server.


> TIP:
>
> If your gpg doesn't survive the following command, chances are, your gpg
> is not available on the MIT Server:
> `$ gpg --keyserver pgp.mit.edu --search-keys <email or name>`

There should be a `Signed-off-by: Full Name <email>` in every of your commit. A
proper `signed-off` commit with GnuPG message is something as such, using
`git log --show-signatures`:
```
commit 01b2208172fc880fbd203fa19f61a84edb546169
gpg: Signature made 2018-07-28T18:23:40 +08
gpg:                using RSA key <local key ID>
gpg: Good signature from "(Holloway) Chew, Kean Ho (ZORALab developer) <kean.ho.chew@zoralab.com>" [ultimate]
Author: (Holloway) Chew, Kean Ho <kean.ho.chew@zoralab.com>
Date:   Sat Jul 28 18:22:22 2018 +0800

    coding_guidelines/yml.md: updated yml to latest consistency

    The existing guideline is out-of-date and some are complicated.
    yml file should be self-explainatory and consistent for simplicty
    sake.

    This patch updates the guideline to the latest, keeping things
    easier to use.

    Signed-off-by: (Holloway) Chew, Kean Ho <kean.ho.chew@zoralab.com>
```

<br/>

### Commit Format
There is absolutely not reason for you not to write a good commit message. A
good commit **only holds 1 action/purpose**.

<br/>

#### Column width
Keep it `80` columns maximum. It's a terminal-based system after all.

<br/>

#### Keep it Small
Just like before, keep your commit small, as in **only holds 1 action/purpose**.
We got enough work on our plate already and don't make us to research your
patch (left alone reviewing your codes).

It's very easy to sniff out a big commit: the use of **AND** in your short line.

Here is an example of a big commit:

```
name/file - add feature A and feature B into repository
```

Here what should have done (2 commits):

```
name/file - add feature A into repository
name/file - add feature B into repository
```

<br/>

#### Write as a Patch
Do you know each commit can be a patch file? There is such command that can
create it for you:
```
# generate
$ git format-patch -p1 <commit_id> -o /path/to/output/directory

# import
$ git am --signoff /path/to/patches/*.patch
```
Hence, this is a reason why you should write your commit message as clear,
crisp, and out of dependency.

A good commit message should have the following pattern:
```
<WHERE>/<FILENAME> - WHAT IS THIS PATCH ABOUT MAX 75 WIDTH

Explain WHAT is the problem? How big is the IMPACT?
Include data like system dump here as well. Just
write it as clear and detailed as possible.
It is perfectly fine to have a long commit than asking
me to read things over all the places!

In new paragraph, explain WHY we need to fix it.
Why must we apply this patch. How to we approach the
problem. Detailed your reasoning and an overview
of your efforts (but don't have to explain the codes).

In new paragraph, explain what's is patch does in a
nutshell. Don't explain the codes. We can read them  using
`git diff`. Remember to sign the commit so the best
command would be `git commit -s`.

Signed-off-by: FULLNAME <email>

```
So please! That's our only request.

<br/>

## Process
### Step 1 - Always File an Issue
The very first step is file an issue in the
[Issues](https://gitlab.com/ZORALab/fennec/issues) section. All discussions
should be ready and maintainers are aware and ready for your changes.

> ***Goal*** - avoid shocking surprises. Life has a lot of them.

<br/>

### Step 2 - Setting Up
Once the idea is accepted, you may proceed to fork out a local version and work
on it. Normally, we work on `next` branch so it's safe to use the following
command pattern:
```
$ git clone -b <next branch> <URL>
```

Please ensures you setup the necessary continuous integration system following
the project description. If you're using GitLab CI and the repository is
configured to use shared runner. You're in luck (since you don't need to do
anything).

> ***Goal*** - synchonize the local setups.

<br/>

### Step 3 - Development
You may begin your development. As you develop your codes, we gently remind you
that:

1. Comply to our Upstream process ground rules.
2. Comply to the project's [Coding Style]({{< absLangLink "/specifications/coding-style" >}}).
3. Keep your commit small (without using `and` in your short-line).
4. Remove secret files.
5. Sign your commit with GnuPG.

<br/>

### Step 4 - Test Your Code
At the very last commit, please **TEST** your code. The definitions of done are:

> 1. New code developments are completed **AND**
> 2. Test script is completed, tested **AND**
> 3. Documentations are updated **AND**

<br/>

### Step 5 - Remove Secrets
This step is optional if you do your Step 3 properly. However, please do it
as a last check before commiting out to the public / network access.

<br/>

### Step 6 - Rebase with Remote `next` Branch
Chances are, by the time you completed your project, the remote `next` branch
is having the latest and greatest. Hence, you'll need to synchonize with it.
It is quite easy:
```bash
$ git remote add upstream <remote original branch>
$ git fetch upstream
$ git rebase upstream/<remote original branch name>
```

<br/>

### Step 7 - Raise Merge Request
You're ready now. Process to make a pull request. Please make sure you update
your `issue` in the [Issues](https://gitlab.com/ZORALab/fennec/issues) section.
