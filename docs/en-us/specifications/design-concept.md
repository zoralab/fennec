<!--
+++
title = "Design Concept"
description = "Fennec design concept. This explains how Fennec works in a nutshell and its overview."
keywords = ["fennec", "design", "concept"]
authors = [ "ZORALab", "(Holloway), Chew Kean Ho" ]
date = "Wed Feb 27 10:27:37 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 1
+++
-->

# Design Concept
Fennec is specifically designed for reusing any GitLab Continuous Integration
(GitLab CI) executions and codes. It helps by speeding up the CI setup and
have a stronger standardization for CI execution.

Initially designed for that purpose, today, Fennec is upgraded to the point
where local repo developer can executes the automation execution locally.

This way, the project is not under threat when GitLab CI is no longer available
for production and deployment.

<br/>

## The Story
The biggest problem with developing the `.gitlab-ci.yml` file is its
requirements to commit the development code in order to test it. This dirties
up the git log and meantime, committing risky codes into the repository.

Also, as you build multiple projects, these executions are more or less the
same across the projects. There is no point copy-pasting `.gitlab-ci.yml` from
one another. Furthermore, it is very hard to roll updates to these CI
executions. Therefore, We need a maintainable way to handle CI execution
scripts, once and for all.

Our leader Holloway decided to take a look at GitLab CI documentation and
decided to make it an application. Appearently, GitLab CI allows user to
`include` other `.gitlab-ci.yml` scripts.

</br>

### The Bento Vending Machine Analogy
That sparkles the idea to group the `.gitlab-ci.yml` fragments into a nicely
organized configuration directories, something like a Japanese bento vending
machine.

The analogy is as such:

> 1. The focus is to make your own bento (`.gitlab-ci.yml`).
> 2. It's all about `include` this *dish* (code blocks), `include` that *dish*,
> and goes on until you finally form the lunch bento you wanted!
> 3. Once you get your bento out, you can add your flavoring like
> *miso and wasabi* (custom/modification code blocks) in your bento.

This analogy is best modelled for Fennec since:

1. All dishes are updatable via the vending machine (distributable).
2. Each dishes in various kind of custom bentos are the same (modularity).
3. Customer can still modify the flavors (encapsulation).

<br/>

## Design Principle #1 - Autopilot OR Semi-Automatic
***The best automation process is semi-automatic. Fennec must be able to execute
on both manual and auto-pilot mode.***

### Rationale
#### Keeping Things Risk Free
However, one key learning from using Github + Circle CI is that the CI
infrastructure can changes drastically, sometimes to the point where the CI
is not available. It can be caused by changes like pricing, budgets,
operations, aquisitions, etc.

It is also not surprising that GitLab CI can have such threat as well. Hence,
Holloway decided that Fennec to carry an independent mindset where project
should be executable even in a local machine, be it VM or physical.

#### No Excuse for Testing
It forces developers not to find excuses for skipping their code testing when
they contribute back to the project. All automation scripts must be executable
independently, locally and remotely as advertised.

#### Work on Product, not Automation Tools

<figure align="center">
	<img src="https://lh3.googleusercontent.com/Gw-myTYIc1mj9ao3kA4NNcC-nAaJlWPyAP6RakORxJLiRdqv3TajzOi27QZjaFG-V-j0VeKnSHLWehJvL7MOH1I1MhRbDx8zfmZnnDTw2E8xFxxym6mnccVYSBU3FkwsiAaY7JvvZTUZcz1Rbry_qSdcIQzBC_8jP1kn41DMjBp9bv9F8s9KwK1PHt9ZfX9M3K3Mo8bK4ifAW-Qc58VV0Yhs_57Lcjm5LodDIs3lItXQCXSJYke7DppE5z6i6owKvAZDQT4BNOG_T8JDmnR48haL2CKXzoXse4x5AaZjLId8671sIB7ka22AI0YkkQTkABLcXs4DobLnL1cXOUuYg1585W5MGj1xMs3QuvIoW0JyWMWacwfRv_ySe73rgwm2nYWjl2r_eFsfeXVTRHlc-FFWHfO-VqjxwBUcVVeE4H3oC0TUewd1-m6N94jbpXdvgLLj71_dPUpRwBy58brTO8AQlvrBs8izLm4kKncfKCPdFALdybBqU-hJMJVYvD9gYybMdfcHhoLwNz_a7xMOlB5280XYwwq9cV0l9QLTv5l_ILsIrh4r5ZmWj-81slITnr624SOYxA9qRQgI_wmkNF7G02NN3jEC5hI0ewVWnqae5JGePpbkf4tX0IlHUbg9phKSKWuSW24kifnexMH8waBlQXvZFtZdV2bMobzm2mjPtrr45uGCi9ywwjCjObOWz5XwIcenEweHYjF0qcN3DpoYHw=w404-h408-no" alt="automation-jokes" />
	<figcaption>
		Comic sourced from: https://xkcd.com/1319/, March, 01, 2019
	</figcaption>
</figure>

The data in this comic stays true. You want to spend most of the time for the
**original tasks that profits**, not on the automation tool (in this case,
CI script). Fully automated scripts ***waste everyone's time*** with each
internal development testing (excluding official testings) so make sure your
process blocks/functions has ways to test and manually interactable.

> **Analogy**
>
> If the sessior is blunt, replace with another one. It's cost effective.
>
> You do not spend large amount of time and resources inventing a sharping
> machine just to sharpen a small sessior.

<br/>

## Design Principle #2 - User Experience First
***Don't do something you are not even interested or passionate about.***

### Rationale
#### Make it Simple to Use
Fennec is targeted to serve git repository independently across multiple
operating system. Hence, keep the user interface (be in command-line or GUI)
simple.

When I say simple, I mean:

1. Inituively easy to issue a command
2. Objectively serving
3. Don't need to go through the entire specifications or read books and
manuscripts
4. Write documents that matters, for people to read, **not for sake of writing***.

> **Analogy**
>
> Do not cook something you yourself would not want to eat. Others won't either.
>
> The only thing you get back is an entry to the blacklist for cook.

<br/>

## Design Principle #3 - Modular and Portable
***Every great things starts small; a sophisticated jet fighters is assembled
from multiple system modules, working independently from one another.***

### Rationale
#### Mission Critical Considerations
Fennec is deployable to mission critical application or software. Hence, each
CI execution code blocks or package must be independent to another. If there
are dependencies, they should be sorted out independently by that package or
code block as well.

#### Facilitate Design Principle #1
Semi-automatic system has to be modular and portable. The keyword is
**assemble together**. Some users will extract the system module and execute
it locally.

> **Analogy**
>
> If a jet fighter got struck by the lightning, it does not go into full reboot
> and crash.
>
> The system only restarts the necessary faulty modules to quickly restore the
> full jet fighter's capabilities.

<br/>

## Wrapping Up
With these concepts in hand, Fennec is born.
