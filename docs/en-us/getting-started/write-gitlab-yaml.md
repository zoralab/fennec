<!--
+++
title = "Writing GitLab-CI YAML"
description = "Next thing to do after setting up Fennec in your repository? Writing your GitLab CI YAML file (.gitlab-ci.yml)"
keywords = ["fennec", ".gitlab-ci.yml", "write", "getting started"]
authors = [ "ZORALab Enterprise", "(Holloway) Chew, Kean Ho" ]
date = "Tue Feb 26 14:59:56 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Getting Started"
weight = 3
+++
-->

# Writing GitLab-CI YAML File
Now that the Fennec is in your repository, it's time to use it. Before we get
our hands dirty, we need to understand briefly on how things work first.

<br/>

## Concept (Brief)
The idea is to leverage the GitLab overriding protocol introduced by using the
`include` keyword in the [GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/#include). What happens is that when including multiple `.yml` file:

1. If the same component specified 2 levels of `.yml` recipes, the latter
**overrides** the former.
2. If the latter `.yml` recipe mentions a new component, it is **added** into
the former.

> *Confusing?*
>
> Simply put, the latest `.yml` overrides similar fields or add if missing.

<br/>

### So How?
Hence, for Fennec, the design is to include 2 stages of compulsory `.yml` file
and then the tasklets or tasks as many as you want. Here is a brief example:
```yaml
include:
    - .fennec/core/linux/debian.yml
    - .fennec/core/linux/base/go.yml
    - .fennec/test/linux/none/all-any.yml
    - .fennec/package/linux/none/all-any.yml
    - .fennec/upstream/linux/none/all-any.yml
    - <other optional yml instructions>
```

The very standard `.gitlab-ci.yml` is your last resort, holding all your custom
implementation.

> **NOTE**
>
> Fennec uses `test` > `package` > `upstream` pipelines. By default, it returns
> an error command and fail the pipelines.
>
> Therefore, you need to acknowledge it by including the `none` recipe for each
> stages.

<br/>

## Recipes
Here are some recipes examples to match your needs. If you want to know more
about the recipes, you can refer to the **recipes - XYZ** section (COMING SOON).

### Debian - Testing Go Module only
In this case, we want the latest Debian image, base for Go programming language:
```yaml
include:
    - .fennec/core/linux/debian.yml
    - .fennec/core/linux/base/go.yml
    - .fennec/test/linux/go/all-mod.yml
    - .fennec/package/linux/none/all-any.yml
    - .fennec/upstream/linux/none/all-any.yml
```
<br/>

### Debian - Testing Go - Add Tasklet
In this case, we want the latest Debian image, base for Go programming language,
and add an optional tasklet.
```yaml
include:
    - .fennec/core/linux/debian.yml
    - .fennec/core/linux/base/go.yml
    - .fennec/test/linux/go/all-mod.yml
    - .fennec/package/linux/none/all-any.yml
    - .fennec/upstream/linux/none/all-any.yml
    - .fennec/tasklet/renew-ssl/letsencrypt/cloudflare/debian.yml
```

<br/>

### Debian - Testing Go - Add Tasklet - Add Hugo Publisher
In this case, we want to add to add Hugo publishing recipe on top of previous.
Therefore, we just add:
```yaml
include:
    - .fennec/core/linux/debian.yml
    - .fennec/core/linux/base/go.yml
    - .fennec/test/linux/go/all-mod.yml
    - .fennec/package/linux/none/all-any.yml
    - .fennec/upstream/linux/none/all-any.yml
    - .fennec/publish/linux/hugo/debian.yml
    - .fennec/tasklet/renew-ssl/letsencrypt/cloudflare/debian.yml
```

<br/><br/>

## Searching YAML files
You might be wondering, how do I search for all these yaml files? The easiest
way is to use `--list` argument alongside `--os-family` to get a list of
available recipes. Here is an example for listing `core` with `linux` os
family:
```bash
$ fennec --list core --os-family linux
./.fennec/core/linux/debian.yml
$
```
To learn more about each, you can either read the documentation here if
available or the configuration file on your own.
