<!--
+++
title = "Project Status"
description = "Want to know more about Fennec current status? This is the right page."
keywords = ["fennec", "project", "status"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Mon Mar  4 16:08:56 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Projects"
weight = 1
name = "Status"
+++
-->
# Project Status
Here is the current project status for Fennec project.

<br/>

## Sources Location
| Name          | Location    |
|:--------------|:------------|
| GitLab.com    | [https://gitlab.com/ZORALab/fennec](https://gitlab.com/ZORALab/fennec) |


<br/>

## Development Health
| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | ![pipeline status](https://gitlab.com/ZORALab/fennec/badges/master/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/fennec/badges/master/coverage.svg) |
| `next`   | ![pipeline status](https://gitlab.com/ZORALab/fennec/badges/next/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/fennec/badges/next/coverage.svg) |

<br/>

## Release Health
![release](https://img.shields.io/badge/release%20quality-alpha-red.svg?style=for-the-badge)

![latest-commit](https://img.shields.io/badge/latest%20commit-0dbdade6fe85759ecb97f6401a7b88f5769c211e-1313c3.svg?style=for-the-badge)

| Release Location  | Location    |
|:------------------|:------------|
| Snapcraft.io      | [https://snapcraft.io/fennec](https://snapcraft.io/fennec) |
| Launchpad.net     | [https://launchpad.net/~zoralab/+archive/ubuntu/fennec](https://launchpad.net/~zoralab/+archive/ubuntu/fennec)

<br/>

## Primary Tools
![language-1](https://img.shields.io/badge/core%20language-Bash-1313c3.svg?style=for-the-badge)
![language-2](https://img.shields.io/badge/core%20language-YAML-1313c3.svg?style=for-the-badge)
![tool-1](https://img.shields.io/badge/core%20tool-BaSHELL-1313c3.svg?style=for-the-badge)
