<!--
+++
title = "License"
description = "Want to know about Fennec License? It's Apache 2.0."
keywords = ["fennec", "license", "apache-2.0"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Sat Mar  2 13:03:48 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Projects"
weight = 3
+++
-->

# License
Fennec is copyrighted under ZORALab Enterprise's Apache 2.0 License, which is
essentially Apache 2.0 License.


## Apache 2.0
```
This project is copyrighted by ZORALab Enterprise under Apache 2.0 License.

To find the license body, please visit:

https://gitlab.com/ZORALab/Resources/raw/master/licenses/apache_2_0
```
