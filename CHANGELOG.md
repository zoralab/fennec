# Version 1.4.0
## Thu, 14 Mar 2019 16:13:10 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 67f26c0 docs/*/getting-started/writing-gitlab-yaml.md: updated to use include
2. d057125 .sites: update the generator to match v1.4.0 bissetii local repo
3. 0a532d1 .gitlab-ci.yml: update to use .fennec no-action for unused stages
4. ccc2faa .fennec/*/linux/none/all-any.yml: added no action recipes
5. b13fdc2 docs/*/specifications/coding-style.md: updated correct unhide codes
6. ff7f48d docs/en-us/specifications/logo.md: fix typo issue with logo size
7. 0e4c2e4 docs/en-us/specifications: fixed broken links
8. 0979c97 docs/en-us/*/sponsoring.md: park sponsoring.md in projects category
9. 1f802d9 docs/en-us/_index.md: set the page to publish
10. b0f13b7 docs/en-us/specifications/sponsoring.md: amended metadata
11. 7304320 docs/en-us/getting-started/write-gitlab-yaml: updated search command
12. 41e0bf5 .sites/themes/.themes: use version 1.2.0 bissetii theme

# Version 1.2.0
## Sat, 09 Mar 2019 19:45:47 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. c86d109 .sites/themes/.themes: set to use v1.1.0
2. 4e9cb45 .sites: updated to use bissetii's version 1.0.0
3. fb26ef4 .fennec/core/linux/scripts/installs.d/hugo: added extended package
4. cfe68d3 docs/en-us/projects/status.md: remove empty spacing at badges

# Version 1.1.0
## Thu, 07 Mar 2019 18:53:23 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 6861902 .sites/themes/.themes: updated to use v0.6.0
2. 055b625 .fennec/*/hugo_build.sh, .sites/manager.sh: fix build --minify issue
3. c14d25c docs/en-us/*/install.md,status.md: added release location
4. 8460289 docs/en-us/*: add repository links to landing page and project status
5. 613a6e2 root/*.md, root/LICENSE: updated to redirect towards website and docs

# Version 1.0.0
## Thu, 07 Mar 2019 13:00:41 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. b3c36ec .gitlab/merge_request_templates: added Checklist into repository
2. 40cf232 .gitlab/issue_templates: added issue templates from ZORALab tags
3. db2965f .fennec/test/linux/go/*: added go test scripts and executions
4. 168b3e9 docs/en-us/specifications/file-structures.md: Updated for test section
5. d7fc6c5 .fennec/*tasks*/*: created placeholder directory for all tasks
6. 71c87ad fennec.sh: added list function for recipes searching
7. b2d3e4b .fennec/*/*: added .gitkeep to all critical directories
8. d777dfe test/scripts/fennec_sh: rename test suite for fennec.sh
9. 528daa1 .fennec/publish/linux/*/*: updated scripts path
10. 8e76aa3 .gitlab-ci.yml: updates publish recipe path
11. 49ea1e9 docs/en-us/specifications/file-structures.md: update core scripts
12. b15d4a2 .fennec/publish/*: updated file structures for standardizations
13. bea3aa0 .sites/config/_default/params.toml: set thumbnailURL to use s862.png
14. d0e5447 .fennec/core/linux/*/go: change environment variable to GO_VERSION
15. f1b5447 docs/en-us/specifications/sponsoring: updated TNC link
16. 60c369e .sites/themes/.themes: update to use v0.5.0
17. 3e585ed docs/en-us/*: remove all invalid thumbnailURL
18. 93a635e .sites/themes/.themes: updated to use v0.4.0 bissetii
19. 1b13dec .sites/config/_default/config.toml: corrected baseURL
20. d4dcfa2 .sites/manager.sh: fixed theme directory installation bug
21. 13f4d16 .fennec/core/linux/scripts/installs.d: added git installer script
22. efbf22a .fennec/publish/hugo/debian.yml: update SITE_PATH to use .site
23. 634349d .fennec/core/linux/scripts/installs.d: added 00_sudo.sh
24. d472cb0 .fennec/core/linux/scripts/installs.d: fix cURL installation issues
25. b9569d4 .gitlab-ci.yml: added local hugo publishing recipes.
26. 8dd35af .fennec/publish/hugo/debian.yml: set hugo publish to watch next branch
27. 1dbfc0d .fennec/core/linux/scripts: renamed installs directory to scripts
28. e670578 .fennec/core/linux/base: added hugo.yml for base installation
29. 11f1718 .sites: added documentation sites generator into repository
30. abc3dfb docs: added fennec documentations into repository
31. 9eb2be0 .sites: disabled zh_cn language
32. 78c639e .fennec/tasklet/renew-ssl/letsencrypt/cloudflare: refactored run.sh
33. 89c3a3b .fennec/core/linux/*: refactored core/linux
34. 848286a .fennec/publish/*: updated .fennec publish recipes
35. 5f7f062 .fennec/core/linux/installs/installs.d/*: added missing setup scripts
36. 9363d48 .gitignore: added exclusion not to ignore *.d/* directory
37. 412d706 .sites: added Hugo based document website generator skeleton
38. 94317ad manta: updated package to respect .fennec naming update
39. f2189c4 .fennec/core/linux/installs/setup.sh: corrected bash install
40. 0a42277 test/scripts/fennec/api/gitlab/pages_domains/*: fixed location issues
41. 329d44f .fennec/*: updated to check sudoer instead as variable
42. 74e66cf .fennec/core/linux/installs/*: updated to use whoami over $USER
43. 30bbcd2 .fennec/core/linux/installs/setup.sh: corrected typo
44. f253381 bashell.sh: updated bashell to version 1.4.2
45. b1ade05 tests/scripts/*: updated to match .fennec directory
46. b782c74 .gitlab-ci.yml: updated to match .fennec directory naming
47. c2f1629 .fennec: move fennec directory into .fennec format
48. 9fc64ba scripts/manta: updated to enable upstream functionalities

# Version 0.5.0
## Fri, 07 Sep 2018 17:14:30 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. d0b3942 fennec.sh: update software version to 0.5.0
2. 323ba8a README.md: updated steps and usage for fennec
3. 49d31da manta/manta.cfg: added debian stable branch package
4. 5d0667a fennec/api/gitlab/config.sh: change to executable permission
5. cbc1e17 manta: added manta packaging software into repository
6. cd12e07 fennec.sh: fixed CI test failure
7. 5beac7e fennec.sh: added main interactor program fennec.sh into repository
8. 24528f1 fennec/core/linux/debian.yml: added PATH designation for local ./bin
9. 6cb52cd .gitlab-ci.yml: updated for new fennec structure
10. 42f638a fennec/publish/hugo: added debian hugo publisher
11. db2dd1d fennec/linux: shifted core components into its core folder
12. 571c3a7 fennec/publish/dump/debian: shifted pages task into its own modules
13. 855810d .gitlab-ci.yml: added CI support for main branch
14. fdbce7a fennec/linux/core-debian.yml: fix TASK_TYPE invalid syntax
15. df04c0d fennec: move out from src folder
16. c026248 fennec/api/gitlab/pages_domains.sh: added test scripts
17. ca3b917 bashell: installed bashell test framework
18. 4f9f9e7 .gitlab-ci.yml: remove all existing CI configurations
19. 10c4320 src/fennec/tasklet/renew-ssl: added letsecrypt-cloudflare for debian.yml
20. 7c0884f src/fennec/linux: added base and core partial framework
21. 56d196d src/fennec/api: added gitlab base api script for GitLab
22. f52b963 src/gitlab: removed initial artifacts
23. 148eb76 .gitlab-ci: updated beta release .gitlab-ci.yml version
