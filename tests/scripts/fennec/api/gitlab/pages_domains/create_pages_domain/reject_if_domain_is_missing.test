#!/bin/bash
TITLE='create_pages_domain is able to reject running when domain is missing.'
SUITE='fennec/api/gitlab/pages_domains/create_pages_domain'
DESCRIPTION='
The create_pages_domain is able to reject calling GitLab API if domain is
not given.
'
tprint_test_description "$TITLE" "$SUITE" "$DESCRIPTION"
################################################################################
tprint "prepare fennec into test directory"
cp -r "$CURRENT_DIRECTORY/.fennec" "$TEST_TEMP_DIRECTORY/."
HOME="$TEST_TEMP_DIRECTORY/username"
mkdir -p "$HOME"
cd "$TEST_TEMP_DIRECTORY"

GITLAB_API_URL="https://gitlab.com/api/v4"
GITLAB_PRIVATE_TOKEN="abcdef123--abcdef123"
gitlab_id="groupname%2Fprojectname"
domain_value="www.example.com"
ssl_pem_path_value="$TEST_TEMP_DIRECTORY/ssl.pem"
ssl_key_path_value="$TEST_TEMP_DIRECTORY/ssl.key"
cp "$TEST_SCRIPT_DIRECTORY/fennec/api/gitlab/pages_domains/dummy_ssl.cert" \
	"$ssl_pem_path_value"
cp "$TEST_SCRIPT_DIRECTORY/fennec/api/gitlab/pages_domains/dummy_ssl_key" \
	"$ssl_key_path_value"
error_message="[ ERROR ] missing domain"

tprint "testing with domain; without ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" == *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing with domain; with ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
ssl_pem_path="$ssl_pem_path_value"
ssl_key_path="$ssl_key_path_value"
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" == *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing with empty domain; without ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain=""
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" != *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing with empty domain; with ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain=""
ssl_pem_path="$ssl_pem_path_value"
ssl_key_path="$ssl_key_path_value"
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" != *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing without domain; without ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
unset domain
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" != *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing without domain; with ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
unset domain
ssl_pem_path="$ssl_pem_path_value"
ssl_key_path="$ssl_key_path_value"
curl() {
	:
}
ret=$(create_pages_domain)
if [[ "$ret" != *"$error_message"* ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

verdict --passed
exit $TEST_PASS
