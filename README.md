[![language](https://img.shields.io/badge/core%20language-Bash%2C%20YAML-1313c3.svg?style=for-the-badge)]()
[![license](https://img.shields.io/badge/License-APACHE%202%2E0-orange.svg?style=for-the-badge)]()
[![release](https://img.shields.io/badge/release%20quality-alpha-red.svg?style=for-the-badge)]()

<!--

[![release](https://img.shields.io/badge/release%20quality-beta-blue.svg?style=for-the-badge)]()
[![release](https://img.shields.io/badge/release%20quality-gold-yellow.svg?style=for-the-badge)]()

-->

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | [![pipeline status](https://gitlab.com/ZORALab/fennec/badges/master/pipeline.svg)]() | [![coverage report](https://gitlab.com/ZORALab/fennec/badges/master/coverage.svg)]() |
| `next`   | [![pipeline status](https://gitlab.com/ZORALab/fennec/badges/next/pipeline.svg)]() | [![coverage report](https://gitlab.com/ZORALab/fennec/badges/next/coverage.svg)]() |

[![Fennec-Banner](https://lh3.googleusercontent.com/WALafehG4Za4drXmKJYRzLsEe3vP1ISTpU46BQcehakuThlxGtiJzvrKSgS_HDde4x1nH0RhVNk0TCggmNGgksdLvcirTu7QGwZZ0FMXZ5uIa-l1Z6ruE23JPfGmaTfzFeMPjfySbY_qIdDKJLOmgCIYvLnYolohb3AY_XCBmw8zEf9QLcleW__qctfsGvtIbTSUiv4UfLowE3TJiEbZWpIrwc9SM7ByM58Ma0oKVlvmsZZ9MtNw1aj7A4voKUP5iksVNi9vEGx3Y_bk9GCwBUbyGGfAVOoPjpzZ-nqkljrhutp7SlO-HqqzKXK-_y40oud6uVCvm6cPQ0wFDUHivMgkLIccyTl969wnuHLfuXU7CUoKIicR8zDDP61RNPZrTIQyRnUpZPzkolDnSQklF9EdZ0XeeOwihVzHK-ibHSPAGJJP5twkVV9Psb0dewjBBn6UqnDjHxGGJjcuNkl_lVmDZbsc6wQX7GOJfOkAWB2cwqLp92lGxPrg2_Rs_YJEGznasCwsp-6t9p2L6qklR9BKzyHWfBe-_ef1YHyFyFzL6yBtyE27ZfPCsqbzFHCFHwQoPr1WqDBZAApbyNdF1-6Zep5JGoLCEV4MtCvK3GKAcEHTT_ctdKW5nC3cLwIwuOgGd3wYsZdiyMblWFKg1mgdQA9NmLT_=w1818-h909-no)]()

# Fennec
Fennec is a software management tool to handle web supported git repository.
It structures the repository for a semi-automatic continuous integration
alongside with web platform supports, such as GitLab.

The goal is to reuse as many `.gitlab-ci.yml` codes as possible to:
1. Reduce unnecessary CI related commits just for testing.
2. Cleaner `.gitlab-ci.yml` recipe.

For more information, visit Fennec's official website at:
[https://zoralab.gitlab.io/fennec/en-us/](https://zoralab.gitlab.io/fennec/en-us/)

<br/>

*This project is sponsored by:*

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)
